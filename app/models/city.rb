class City < ApplicationRecord
  # after_create_commit { CityBroadcastJob.perform_later(self) }

  # Constants
  CITIES = {
    'CL' => { name: 'Santiago', latitude: -33.44889, longitude: -70.669265 },
    'CH' => { name: 'Zurich', latitude: 47.376687, longitude: 8.541694 },
    'NZ' => { name: 'Auckland', latitude: -36.84846, longitude: 174.76332 },
    'AU' => { name: 'Sydney', latitude: -33.86882, longitude: 151.209296 },
    'UK' => { name: 'Londres', latitude: 51.507351, longitude: -0.127758 },
    'USA' => { name: 'Georgia', latitude: 32.165622, longitude: -82.900075 },
  }

  ICONS = [
    { id: 'clear-day', icon: 'wi wi-day-sunny'},
    { id: 'clear-night', icon: 'wi wi-night-clear'},
    { id: 'rain', icon: 'wi wi-rain'},
    { id: 'snow', icon: 'wi wi-snow'},
    { id: 'sleet', icon: 'wi wi-sleet'},
    { id: 'wind', icon: 'wi wi-strong-wind'},
    { id: 'fog', icon: 'wi wi-fog'},
    { id: 'cloudy', icon: 'wi wi-cloudy'},
    { id: 'partly-cloudy-day', icon: 'wi wi-day-cloudy'},
    { id: 'partly-cloudy-night', icon: 'wi wi-night-alt-cloudy'},
  ]

  # Class methods
  def self.action_cable code
    city_weather = api_request code
    ActionCable.server.broadcast 'city_channel', { code: code, weather: render_message(city_weather) }
  end

  def self.api_request code
    city = $redis.hget(:cities, code)
    city = JSON.load city
    begin
      unless rand_error(0.0, 1.0, 0.1) # min, max, pct
        # Api request
        url = URI.parse "https://api.darksky.net/forecast/bea17e20bcb5d9f82603ae0644f92708/#{city['latitude']},#{city['longitude']}"
        request = Net::HTTP.get url
        request = JSON.load request

        # Set icon
        icon = City::ICONS.find { |icon| icon[:id] == request["currently"]["icon"] }

        # Set timezone
        Time.zone = request["timezone"]

        city_weather = { code: code, name: city["name"], datetime: Time.zone.at(request["currently"]["time"]), temperature: request["currently"]["temperature"], apparent_temperature: request["currently"]["apparentTemperature"], icon: icon[:icon] }
      else
        raise ApiError::FailedRequest.new code
      end
    rescue ApiError::FailedRequest => e
      # Catch error in redis
      $redis.hset('api.error', Time.now.to_i, { class_error: e.class.to_s, message: e.message, code: e.code, backtrace: e.backtrace.first })
      retry
    end
    return city_weather
  end

  def self.render_message weather
    ApplicationController.renderer.render(partial: 'city/current_weather', locals: { weather: weather })
  end

  # random error
  def self.rand_error min, max, pct
    return rand(min .. max) < pct
  end
end
