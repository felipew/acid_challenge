class CityChannel < ApplicationCable::Channel
  def subscribed
    stream_from "city_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def request(data)
    City.action_cable data['code']
  end
end
