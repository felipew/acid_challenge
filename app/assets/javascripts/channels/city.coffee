App.city = App.cable.subscriptions.create "CityChannel",
  connected: ->
    # Called when the subscription is ready for use on the server
    setInterval () ->
      list = document.getElementsByClassName("cities-weather")
      for li in list
        App.city.request li.id
    , 10000


  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    $("#" + data['code']).replaceWith(data['weather'])

  request: (code) ->
    @perform 'request', code: code
