module ApiError
  class FailedRequest < StandardError
    attr_reader :code, :message

    def initialize(_code = nil, _message = nil)
      @code = _code
      @message = _message || 'How unfortunate! The API Request Failed.'
      super(_message)
    end
  end
end
