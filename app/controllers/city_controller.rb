class CityController < ApplicationController
  # GET /city/index
  def index
    @current_weather = []
    City::CITIES.each do | city |
      @current_weather << City.api_request(city.first)
    end
  end

end
